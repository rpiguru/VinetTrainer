from utils.store import StoreGetSetter


class TargetPath(StoreGetSetter):
    key = 'target_path'
    default_for_missed = ''
