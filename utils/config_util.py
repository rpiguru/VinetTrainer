"""
    Utility for configuration
"""
try:
    import ConfigParser as conf
except ImportError:
    import configparser as conf

import os

config = conf.RawConfigParser()
config_file = os.path.expanduser('~/.vinet.ini')


def get_config(section, option, default_value=None):
    """
    Get configuration value
    :param section:
    :param option:
    :param default_value:
    :return:
    """
    config.read(config_file)
    try:
        val = config.get(section=section, option=option)
        if type(val) == list:
            val = val[0]
        return val
    except conf.NoSectionError:
        return default_value
    except conf.NoOptionError:
        return default_value


def set_config(section, option, value):
    if type(value) != str:
        value = str(value)

    config.read(config_file)
    if section not in config.sections():
        config.add_section(section=section)
    config.set(section, option, value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except IOError:
        return False
