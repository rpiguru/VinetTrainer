"""

    Common utilities

"""
from utils import config_util


def is_debug_mode():
    cur_mode = config_util.get_config('general', 'debug', 'OFF')
    is_debug = True if cur_mode.lower() == 'true' else False
    return is_debug


def get_theme_color():
    theme = {
        'primary_palette': config_util.get_config('theme', 'primary_palette', 'Blue'),
        'accent_palette': config_util.get_config('theme', 'accent_palette', 'Amber'),
        'theme_style': config_util.get_config('theme', 'theme_style', 'Light'),
    }
    return theme


def set_theme_color(val):
    for _key in val.keys():
        config_util.set_config('theme', _key, val[_key]),


def get_human_file_size(byte_size, units=[' bytes', 'KB', 'MB', 'GB']):
    return str(byte_size) + units[0] if byte_size < 1024 else get_human_file_size(byte_size >> 10, units[1:])
