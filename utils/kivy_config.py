"""
    Pre-configuration file to be used to setup Kivy configurations.
    This module must be called before executing a Kivy GUI app!

"""
import os
from kivy.clock import Clock
from kivy.config import Config

Config.read(os.path.expanduser('~/.kivy/config.ini'))
Config.set('graphics', 'width', '1280')
Config.set('graphics', 'height', '800')
Config.set('kivy', 'keyboard_mode', 'system')


Clock.max_iteration = 20
