from copy import deepcopy
from kivy.weakmethod import WeakMethod
from utils.singleton import Singleton


class StoreGetSetter:
    key = None  # Store key for default implementation of get and set methods
    default_for_missed = None  # default value, if key is not presented in Store

    def __init__(self, key=None, default_for_missed=None):
        if key:
            self.key = key
        if default_for_missed:
            self.default_for_missed = default_for_missed

    def get(self):
        return Store().get_value(self.key, self.default_for_missed)

    def set(self, value):
        Store().set_value(self.key, value)

    def clear(self):
        Store().remove_value(self.key)

    def subscribe(self, callback):
        Store().subscribe(self.key, callback)

    def set_default(self):
        Store().set_value(self.key, self.default_for_missed)


class Store(object, metaclass=Singleton):
    _callbacks = {}
    """ dict for callbacks storing
        key - store section name
        value - array of kivy's WeakMethods
    """

    def set_value(self, key, value):
        setattr(self, key, deepcopy(value))
        self._dispatch(key)

    def _dispatch(self, key):
        if key in self._callbacks:
            for weak_callback in self._callbacks[key][:]:
                full_callback = weak_callback()
                if full_callback is None or full_callback(self.get_value(key)):
                    # Remove callback if object doesn't exist or callback returned True
                    self._callbacks[key].remove(weak_callback)

    def get_value(self, key, default=None):
        return deepcopy(getattr(self, key, default))  # use only Store().set_value to edit field values

    def has_key(self, key):
        return hasattr(self, key)

    def remove_value(self, key):
        if key in self.__dict__:
            delattr(self, key)
            self._dispatch(key)

    def clear_all(self):
        keys = []
        for key in vars(self):
            keys.append(key)

        for key in keys:
            delattr(self, key)

    def subscribe(self, section_name, callback):
        # set callback to be called when store field is changed
        weak_callback = WeakMethod(callback)
        if section_name in self._callbacks:
            self._callbacks[section_name].append(weak_callback)
        else:
            self._callbacks[section_name] = [weak_callback]
