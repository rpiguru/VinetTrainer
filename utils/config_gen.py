import os

try:
    import ConfigParser as conf
except ImportError:
    import configparser as conf


config = conf.RawConfigParser()
config_file = os.path.expanduser('~/.vinet.ini')


def check_config_file():
    """
    Check config file and generate unless it exists.
    :return:
    """
    config.read(config_file)

    try:
        config.get('general', 'debug')
        is_exist = True
    except (conf.NoSectionError, conf.NoOptionError):
        is_exist = False

    if not is_exist:
        cfgfile = open(config_file, 'w')
        # add the sections/options to the file here
        config.add_section('general')
        config.set('general', 'debug', 'False')
        config.set('general', 'title', 'Vinet Tainer Application')

        config.add_section('vinet')
        config.set('vinet', 'media_path', '~/Pictures')

        config.add_section('theme')
        config.set('theme', 'primary_palette', 'Blue')
        config.set('theme', 'accent_palette', 'Amber')
        config.set('theme', 'theme_style', 'Light')

        config.write(cfgfile)
        cfgfile.close()
