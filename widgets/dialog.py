import os
from kivy.lang import Builder
from kivy.properties import OptionProperty, StringProperty, ListProperty
from kivy.uix.popup import Popup

from kivymd.dialog import MDDialog
from utils.config_util import get_config

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class FileChooserDialog(Popup):
    mode = OptionProperty('file', options=['file', 'dir'])
    extensions = ListProperty()
    path = StringProperty('')

    def __init__(self, **kwargs):
        super(FileChooserDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        self.path = os.path.expanduser(get_config('vinet', 'media_path', '~/PIG Videos'))

    def filter_content(self, directory, filename):
        if self.mode == 'file':
            if self.extensions:
                return filename.split('.')[-1] in self.extensions
            else:
                return True
        else:
            return os.path.isdir(os.path.join(directory, filename))

    def on_ok(self):
        self.dismiss()
        self.dispatch('on_confirm', self.ids.txt_path.text)

    def on_confirm(self, *args):
        pass

    def selected(self):
        self.ids.txt_path.text = str(self.ids.fc.selection[0])


class MessageBox(MDDialog):
    msg = StringProperty()
