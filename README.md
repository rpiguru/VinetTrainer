# VinetTrainer - Python training GUI application

## Install dependencies

### Install **OpenCV**

- Windows

    https://docs.opencv.org/3.3.1/d5/de5/tutorial_py_setup_in_windows.html

- Ubuntu

    https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/


### Install Kivy (https://kivy.org/docs/installation/installation.html)

- Windows

        python -m pip install --upgrade pip wheel setuptools
        python -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew
        python -m pip install kivy.deps.gstreamer
        python -m pip install kivy

- Ubuntu

        sudo apt-get install -y python-pip build-essential \
            git python python-dev ffmpeg libsdl2-dev libsdl2-image-dev \
            libsdl2-mixer-dev libsdl2-ttf-dev libportmidi-dev \
            libswscale-dev libavformat-dev libavcodec-dev zlib1g-dev xcel xclip

        sudo apt-get install -y libgstreamer1.0 gstreamer1.0-plugins-base gstreamer1.0-plugins-good

        sudo pip install Cython==0.25.2
        sudo pip install kivy


### Install some python packages.

    sudo pip install -r requirements.txt
