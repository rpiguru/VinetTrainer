import calendar
import traceback
import os

import datetime
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import NoTransition, SlideTransition, ScreenManagerException
from kivy.base import ExceptionHandler, ExceptionManager
from kivy.clock import Clock

import utils.kivy_config
import utils.factory_reg


from utils.config_gen import check_config_file
from kivymd.theming import ThemeManager
from kivy.logger import Logger
from screens.screen_manager import screens

from utils.common import get_theme_color
from utils.config_util import get_config
from utils.store_selectors import TargetPath
from widgets.dialog import FileChooserDialog


class VinetTrainerExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        Logger.error('-- Exception: {}'.format(traceback.format_exc(limit=30)))
        _app = App.get_running_app()
        # _app.save_exception(traceback.format_exc(limit=20))
        _app.go_screen('start_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(VinetTrainerExceptionHandler())


class VinetTrainerApp(App):

    theme_cls = ThemeManager()
    root = None
    sm = None

    def build(self):
        """
        base function of kivy app
        :return:
        """
        self.title = get_config('general', 'title', 'Vinet Trainer Application')
        self._apply_theme()
        self.root = Builder.load_file(os.path.join(os.path.dirname(__file__), 'app.kv'))
        self.sm = self.root.ids.sm
        self.switch_screen('start_screen')
        Clock.schedule_interval(self.show_time, 1)
        return self.root

    def _apply_theme(self):
        theme = get_theme_color()
        self.theme_cls.primary_palette = theme['primary_palette']
        self.theme_cls.accent_palette = theme['accent_palette']
        self.theme_cls.theme_style = theme['theme_style']

    def on_btn_open_image(self):
        popup = FileChooserDialog(mode='dir')
        popup.bind(on_confirm=self.start_image_processing)
        popup.open()

    def on_btn_open_video(self):
        popup = FileChooserDialog(mode='file', extensions=['mp4', 'MP4', 'mpg', 'MPG'])
        popup.bind(on_confirm=self.start_video_processing)
        popup.open()

    def on_btn_settings(self):
        self.switch_screen('settings_screen')

    def start_image_processing(self, *args):
        TargetPath().set(args[1])
        self.switch_screen('menu_image_screen')

    def start_video_processing(self, *args):
        TargetPath().set(args[1])
        self.switch_screen('menu_video_screen')

    def switch_screen(self, dest_screen, direction=None):
        """
        Go to given screen
        :param dest_screen:
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        self.sm.transition = NoTransition() if direction is None else SlideTransition(direction=direction)
        if self.sm.has_screen(dest_screen):
            self.sm.current = dest_screen
            self.sm.current_screen.reload()
            return
        else:
            if dest_screen in screens.keys():
                screen = screens[dest_screen](app=self, name=dest_screen)
                self.sm.switch_to(screen, direction=direction)
                return
        raise ScreenManagerException('Vinet: Screen {} not found'.format(dest_screen))

    def show_time(self, *args):
        local_time = datetime.datetime.now()
        str_time = local_time.strftime("%B %d, %Y  %H:%M:%S")
        self.root.ids.lb_time.text = '{}, {}'.format(calendar.day_name[local_time.weekday()][:3], str_time)


if __name__ == '__main__':

    check_config_file()

    app = VinetTrainerApp()
    app.run()
