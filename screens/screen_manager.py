from screens.menu.images_screen import ImageMenuScreen
from screens.menu.video_screen import VideoMenuScreen
from screens.settings.screen import SettingsScreen
from screens.start.screen import StartScreen


# TODO: Add more screens here!

screens = {
    'menu_image_screen': ImageMenuScreen,
    'menu_video_screen': VideoMenuScreen,
    'start_screen': StartScreen,
    'settings_screen': SettingsScreen,
}
