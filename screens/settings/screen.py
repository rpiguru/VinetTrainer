import os
from kivy.clock import Clock
from kivy.lang import Builder
from kivymd.theme_picker import MDThemePicker
from screens.base import BaseScreen
from utils.common import set_theme_color

Builder.load_file(os.path.join(os.path.dirname(__file__), 'settings.kv'))


class SettingsScreen(BaseScreen):

    def on_btn_change(self):
        picker = MDThemePicker()
        picker.bind(on_dismiss=self._save_theme)
        Clock.schedule_once(picker.open)

    def _save_theme(self, *args):
        theme = {
            'primary_palette': self.app.theme_cls.primary_palette,
            'accent_palette': self.app.theme_cls.accent_palette,
            'theme_style': self.app.theme_cls.theme_style
        }
        set_theme_color(theme)
