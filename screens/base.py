from kivy.uix.screenmanager import Screen


class BaseScreen(Screen):
    """
    Basic class of all screens.
    """

    def __init__(self, app=None, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = app

    def switch_screen(self, screen):
        self.app.switch_screen(screen)

    def reload(self):
        pass
