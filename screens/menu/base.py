import os
from abc import abstractmethod
import time
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Line
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, ListProperty
from screens.base import BaseScreen
from utils.store_selectors import TargetPath
from kivy.core.window import Window
from kivy.clock import Clock
from functools import partial


Builder.load_file(os.path.join(os.path.dirname(__file__), 'base.kv'))


class MenuScreenBase(BaseScreen):
    path = StringProperty()
    index = NumericProperty()
    orig_size = [0, 0]

    start_pos = None
    start_time = -1
    end_pos = None

    rect_pos = ListProperty([0, 0])
    rect_size = ListProperty([0, 0])

    def __init__(self, **kwargs):
        super(MenuScreenBase, self).__init__(**kwargs)
        Window.fbind('mouse_pos', self._on_mouse_move)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)

    def on_enter(self, *args):
        super(MenuScreenBase, self).on_enter(*args)
        self.reload()

    def reload(self):
        self.path = TargetPath().get()
        self.ids.rect.canvas.clear()
        self.load_media()

    @abstractmethod
    def display_image(self, file_path=None, texture=None):
        pass

    def _calibrate_image(self, w, h):
        """
        Calibrate the size of the image to be shown.
        :param w:
        :param h:
        :return:
        """
        self.orig_size = [w, h]
        width, height = self.ids.img_box.size
        if w / h > width / height:
            self.ids.img.width = width
            self.ids.img.height = h * width / w
        else:
            self.ids.img.height = height
            self.ids.img.width = w * height / h

    def on_touch_down(self, touch):
        super(MenuScreenBase, self).on_touch_down(touch)
        if self.ids.img.collide_point(*touch.pos):
            if abs(touch.time_end - touch.time_start) > 0.2:
                self.start_pos = touch.pos
                self.start_time = time.time()

    def on_touch_move(self, touch):
        super(MenuScreenBase, self).on_touch_move(touch)
        if self.start_time > 0 and time.time() - self.start_time > .2:
            if self.ids.img.collide_point(*touch.pos):
                self.end_pos = touch.pos
                self.ids.rect.canvas.clear()
                with self.ids.rect.canvas:
                    Color(1, 0, 0)
                    x1, y1 = self.start_pos
                    x2, y2 = touch.pos
                    Line(points=[x1, y1, x2, y1, x2, y2, x1, y2], width=1, close=True)

    def on_touch_up(self, touch):
        super(MenuScreenBase, self).on_touch_up(touch)
        if self.start_time > 0 and time.time() - self.start_time > .5:
            self.on_draw_finished()
        self.start_time = -1

    def _on_mouse_move(self, *args):
        pos = self.ids.img.to_local(args[1][0], args[1][1], True)
        if 0 < pos[0] < self.ids.img.width and 0 < pos[1] < self.ids.img.height:
            x, y = self._get_actual_coord(pos)
            self.ids.lb_cur_pos.text = 'X: {}   Y: {}'.format(int(x), int(y))
        else:
            self.ids.lb_cur_pos.text = ''

    def on_draw_finished(self):
        x1, y1 = self._get_actual_coord(self.ids.img.to_local(self.start_pos[0], self.start_pos[1], True))
        x2, y2 = self._get_actual_coord(self.ids.img.to_local(self.end_pos[0], self.end_pos[1], True))
        self.rect_size = abs(x2 - x1), abs(y2 - y1)
        self.rect_pos = min(x1, x2), min(y1, y2)
        self.ids.box_annotation.opacity = 1
        self.ids.btn_delete.disabled = False
        self.save_rect()

    def _get_actual_coord(self, pos):
        """
        Get actual coordinate in the image file.
        :param pos:
        :return:
        """
        x = pos[0] * self.orig_size[0] / self.ids.img.width
        y = self.orig_size[1] - pos[1] * self.orig_size[1] / self.ids.img.height
        return int(x), int(y)

    def on_btn_delete(self):
        if self.rect_pos:
            self.delete_rect()
            self.ids.rect.canvas.clear()
            self.start_pos = None
            self.start_time = -1
            self.end_pos = None
            self.rect_pos = [None, None]
            self.rect_size = [None, None]
            self.ids.btn_delete.disabled = True
            self.ids.box_annotation.opacity = 0

    def on_pre_leave(self, *args):
        Window.funbind('mouse_pos', self._on_mouse_move)
        super(MenuScreenBase, self).on_pre_leave(*args)

    def restore_selection(self, file_path, size):
        """
        Redraw the rect from the result label file.
        :param size: Actual size of the image frame
        :param file_path: File path of the image frame
        :return:
        """
        self.ids.rect.canvas.clear()
        self.ids.btn_delete.disabled = False
        Clock.schedule_once(partial(self._restore_selection, file_path, size))

    def _restore_selection(self, file_path, size, *args):
        cls, cx, cy, w, h = [float(p) for p in open(file_path, 'r').read().split()]
        self.ids.class_number.set_value(int(cls))
        self.ids.box_annotation.opacity = 1
        self.rect_pos = [int((cx - w / 2) * size[0]), int((cy - h / 2) * size[1])]
        self.rect_size = [int(w * size[0]), int(h * size[1])]

        width, height = self.ids.img.size
        x1 = self.ids.img.x + (cx - w / 2) * width
        y1 = self.ids.img.y + (1 - cy - h / 2) * height
        x2 = x1 + w * width
        y2 = y1 + h * height
        with self.ids.rect.canvas:
            Color(1, 0, 0)
            Line(points=[x1, y1, x2, y1, x2, y2, x1, y2], width=1, close=True)

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'left':
            self.on_btn_previous()
        elif keycode[1] == 'right':
            self.on_btn_next()
        elif keycode[1] == 'spacebar':
            self.on_btn_play_pause()

    @abstractmethod
    def load_media(self):
        pass

    @abstractmethod
    def on_btn_previous(self):
        pass

    @abstractmethod
    def on_btn_play_pause(self):
        pass

    @abstractmethod
    def on_btn_next(self):
        pass

    @abstractmethod
    def save_rect(self):
        pass

    @abstractmethod
    def delete_rect(self):
        pass
