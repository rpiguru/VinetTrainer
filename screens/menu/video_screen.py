import os
from kivy.clock import Clock
from kivy.graphics.texture import Texture
from kivy.logger import Logger
from screens.menu.base import MenuScreenBase
import cv2
from utils.common import get_human_file_size
from widgets.dialog import MessageBox


class VideoMenuScreen(MenuScreenBase):

    cap = None
    length = 0
    clk_capture = None

    def on_enter(self, *args):
        super(VideoMenuScreen, self).on_enter(*args)
        self.ids.slider.bind(value=self._on_slider_value)

    def load_media(self):
        try:
            self.cap = cv2.VideoCapture(self.path)
            self.length = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
            self.ids.slider.max = self.length
            ret, frame = self.cap.read()
            h, w = frame.shape[:2]
            self.ids.lb_target.text = self.path
            self.ids.lb_width.text = str(w)
            self.ids.lb_height.text = str(h)
            self.ids.lb_size.text = get_human_file_size(os.stat(self.path).st_size)
            self.display_frame(0)
        except Exception as e:
            Logger.error('Failed to load video file({}) - {}'.format(self.path, e))
            MessageBox(msg='Failed to import video file.').open()
            self.switch_screen('start_screen')
            return

    def display_frame(self, index):
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, index)
        ret, frame = self.cap.read()
        if ret:
            buf_tmp = cv2.flip(frame, 0)
            buf = buf_tmp.tostring()
            h, w = frame.shape[:2]
            self._calibrate_image(w, h)
            texture = Texture.create(size=(w, h), colorfmt='bgr')
            texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
            self.display_image(texture=texture)
            f_name, ext = os.path.splitext(self.path)
            if os.path.exists(f_name + '-%05d.txt' % self.index):
                self.restore_selection(f_name + '-%05d.txt' % self.index, size=(w, h))
        self.ids.lb_count.text = '{}/{}'.format(int(self.index) + 1, int(self.length))

    def display_image(self, file_path=None, texture=None):
        self.ids.img.texture = texture

    def on_btn_play_pause(self):
        if self.clk_capture is None:
            fps = self.cap.get(cv2.CAP_PROP_FPS)
            self.clk_capture = Clock.schedule_interval(lambda dt: self._play_video(), 1 / float(fps))
            self.ids.btn_play.icon = 'pause'
        else:
            self.clk_capture.cancel()
            self.clk_capture = None
            self.ids.btn_play.icon = 'play'

    def _play_video(self):
        if self.end_pos:
            self.save_rect()
        self.index += 1
        if self.index == self.length:
            self.index = 0
        self.display_frame(self.index)

    def _on_slider_value(self, *args):
        if self.clk_capture is None:
            self.index = self.ids.slider.value
            self.display_frame(self.index)

    def on_btn_previous(self):
        if self.end_pos:
            self.save_rect()
        if self.index > 0:
            self.index -= 1
            self.display_frame(self.index)

    def on_btn_next(self):
        if self.end_pos:
            self.save_rect()
        if self.index < self.length - 1:
            self.index += 1
            self.display_frame(self.index)

    def save_rect(self):
        cat = self.ids.class_number.value
        f_name, ext = os.path.splitext(self.path)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, self.index)
        ret, frame = self.cap.read()
        cv2.imwrite(f_name + '-%05d.jpg' % self.index, frame)
        height, width = frame.shape[:2]
        center_x = (self.rect_pos[0] + self.rect_size[0] / 2.) / float(width)
        center_y = (self.rect_pos[1] + self.rect_size[1] / 2.) / float(height)
        w = self.rect_size[0] / float(width)
        h = self.rect_size[1] / float(height)
        with open(f_name + '-%05d.txt' % self.index, 'w') as f:
            f.write(' '.join([str(cat), str(center_x), str(center_y), str(w), str(h)]))

    def delete_rect(self):
        f_name, ext = os.path.splitext(self.path)
        txt_file = f_name + '-%05d.txt' % self.index
        try:
            os.remove(txt_file)
        except Exception as e:
            Logger.error('Vinet: Failed to remove file({}) - {}'.format(txt_file, e))

        img_file = f_name + '-%05d.jpg' % self.index
        try:
            os.remove(img_file)
        except Exception as e:
            Logger.error('Vinet: Failed to remove file({}) - {}'.format(img_file, e))
