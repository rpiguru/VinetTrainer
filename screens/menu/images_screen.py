import glob
import cv2
from kivy.properties import ListProperty
from kivy.logger import Logger
from kivymd.icon_definitions import md_icons
from screens.menu.base import MenuScreenBase
from utils.common import get_human_file_size
from widgets.dialog import MessageBox
import os

EXTENSIONS = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG']


class ImageMenuScreen(MenuScreenBase):

    images = ListProperty()

    def on_enter(self, *args):
        super(ImageMenuScreen, self).on_enter(*args)
        self.ids.main_box.remove_widget(self.ids.slider)
        self.ids.btn_play.disabled = True
        self.ids.lb_type.text = u"{}".format(md_icons['folder-image'])

    def load_media(self):
        images = []
        for ext in EXTENSIONS:
            images += glob.glob(self.path + '/*.{}'.format(ext))
        if not images:
            MessageBox(msg='Image Not Found').open()
            self.switch_screen('start_screen')
            return
        self.images = sorted(images)
        self.index = 0
        self.display_image(file_path=self.images[0])

    def display_image(self, file_path=None, texture=None):
        self.ids.lb_target.text = file_path
        self.ids.img.source = file_path
        h, w = cv2.imread(file_path).shape[:2]
        self._calibrate_image(w, h)
        self.ids.lb_width.text = str(w)
        self.ids.lb_height.text = str(h)
        self.ids.lb_size.text = get_human_file_size(os.stat(file_path).st_size)
        self.ids.lb_count.text = '{}/{}'.format(self.index + 1, len(self.images))
        f_name, ext = os.path.splitext(file_path)
        if os.path.exists(f_name + '.txt'):
            self.restore_selection(f_name + '.txt', size=(w, h))

    def save_rect(self):
        cat = self.ids.class_number.value
        height, width = cv2.imread(self.images[self.index]).shape[:2]
        center_x = (self.rect_pos[0] + self.rect_size[0] / 2.) / float(width)
        center_y = (self.rect_pos[1] + self.rect_size[1] / 2.) / float(height)
        w = self.rect_size[0] / float(width)
        h = self.rect_size[1] / float(height)
        f_name, ext = os.path.splitext(self.images[self.index])
        with open(f_name + '.txt', 'w') as f:
            f.write(' '.join([str(cat), str(center_x), str(center_y), str(w), str(h)]))
        # Snackbar(text='Saved the rectangle: ({}) -> ({})'.format(self.rect_pos, self.rect_size), duration=1.5).show()

    def delete_rect(self):
        f_name, ext = os.path.splitext(self.images[self.index])
        txt_file = f_name + '.txt'
        try:
            os.remove(txt_file)
        except Exception as e:
            Logger.error('Vinet: Failed to remove file({}) - {}'.format(txt_file, e))

    def on_btn_play_pause(self):
        pass

    def on_btn_previous(self):
        if self.end_pos:
            self.save_rect()
        if self.index > 0:
            self.index -= 1
            self.display_image(file_path=self.images[self.index])

    def on_btn_next(self):
        if self.end_pos:
            self.save_rect()
        if self.index < len(self.images) - 1:
            self.index += 1
            self.display_image(file_path=self.images[self.index])
