import os
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'start.kv'))


class StartScreen(BaseScreen):

    def on_touch_down(self, touch):
        super(StartScreen, self).on_touch_down(touch)
